//package com.flexforce.configure;
//
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
//
//
//@EnableWebSecurity
//public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {
//
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests().antMatchers("/swagger-ui/**","/emp/**").permitAll()
//		.anyRequest()
//		.authenticated().and().httpBasic();
//	}
//
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		System.out.println("i am in configure");
//		auth.inMemoryAuthentication().withUser("ishu").password(passwordEncoder().encode("jain")).authorities("admin");
//	}
//	
//	@Bean
//	
//	public PasswordEncoder passwordEncoder() {
//		return new BCryptPasswordEncoder();
//	}
//
////	@Bean
////	CustomTokenAuthenticationFilter getCustomFilter() {
////		return new CustomTokenAuthenticationFilter();
////	}
//
//	/**
//	 * To stop registering {@link}CustomTokenAuthenticationFilter filter
//	 * automatically.
//	 * 
//	 * @param filter
//	 * @return
//	 */
////	@Bean
////	public FilterRegistrationBean registration(CustomTokenAuthenticationFilter filter) {
////		FilterRegistrationBean registration = new FilterRegistrationBean(filter);
////		registration.setEnabled(false);
////		return registration;
////	}
//
//
//	
//
//}