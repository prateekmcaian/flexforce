package com.flexforce.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.Hibernate;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
public class Employee {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long employeeId;
		
	private String firstName;
	
	private String lastName ;
	
	private Integer age;
	
	@Column(columnDefinition = "Decimal(10,2)")
	private Double salary;
	
	
}
