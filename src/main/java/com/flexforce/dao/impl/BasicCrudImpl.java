package com.flexforce.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.flexforce.dao.BasicCrud;
import com.flexforce.dto.CreateEmployeeDto;
import com.flexforce.entities.Employee;

@Service
public class BasicCrudImpl implements BasicCrud {

	@Autowired
	SessionFactory sFactory;

	@Override
	public String addNewEmployee(CreateEmployeeDto employeeDto) {
		
		Employee employee=new Employee();
		employee.setAge(employeeDto.getAge());
		employee.setFirstName(employeeDto.getFirstName());
		employee.setLastName(employeeDto.getLastName());
		employee.setSalary(employeeDto.getSalary());
		
		Session session = sFactory.openSession();
		Transaction t = session.getTransaction();
		t.begin();
		
		try {
		session.save(employee);
		t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}finally {
			session.close();
		}
		
		return "Saved SuccessFully!!!";
	}

	@Override
	public String deleteEmployee(Long empId) throws Throwable {
		// TODO Auto-generated method stub
		Session session = sFactory.openSession();
		Transaction t = session.getTransaction();
		t.begin();

		Employee employee = (Employee) session.get(Employee.class, empId);

		if (employee == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee Not Found !!!");

		try {
		session.delete(employee);
		t.commit();
		}
		catch (Exception e) {
		t.rollback();
		}finally {
			session.close();
		}
		
		return "Delete SuccessFully!!!";

	}

	@Override
	public Employee getEmployeeById(Long id) throws Throwable {
		// TODO Auto-generated method stub
		Session session = sFactory.openSession();

		Employee employee = (Employee) session.get(Employee.class, id);

		if (employee == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Employee Not Found !!!");
		return employee;
	}

	@Override
	public List<Employee> getEmployeeByName(String name) throws Throwable {

		Session session = sFactory.openSession();
		List<Employee> employees = session.createQuery("from Employee e where e.firstName=:empName")
				.setParameter("empName", name).list();

		return employees;
	}

	@Override
	public String updateEmployee(Employee employee) throws Throwable {
	Session session = sFactory.openSession();
	Transaction transaction =session.beginTransaction();
		
	if (employee.getEmployeeId() == null)
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
					"Please provide Employee id To get Update !!!");
		
		
		Employee employee2 = session.get(Employee.class, employee.getEmployeeId());

		// for whole field update

		/*
		 * try {
		 * 
		 * transaction = session.beginTransaction(); session.update(employee);
		 * transaction.commit();
		 * 
		 * } catch (Exception e) { transaction.rollback(); }
		 */

		// Either We can use if that person want to update specific field
		
		if (employee.getAge() != null)
			employee2.setAge(employee2.getAge());

		if (employee.getFirstName() != null)
			employee2.setFirstName(employee.getFirstName());

		if (employee.getLastName() != null)
			employee2.setLastName(employee.getLastName());

		if (employee.getSalary() != null)
			employee2.setSalary(employee.getSalary());

		try {

			
			session.update(employee2);
			transaction.commit();
			
		} catch (Exception e) {
			transaction.rollback();
		}finally {
			session.close();
		}
		
		return "updated ";
	}

}
