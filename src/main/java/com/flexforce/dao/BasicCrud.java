package com.flexforce.dao;

import java.util.List;

import com.flexforce.dto.CreateEmployeeDto;
import com.flexforce.entities.Employee;

public interface BasicCrud {

	public String addNewEmployee( CreateEmployeeDto employee ) ;
	public String deleteEmployee(Long empId ) throws Throwable;
	public Employee getEmployeeById(Long id ) throws Throwable;
	public List<Employee> getEmployeeByName(String name ) throws Throwable;
	public String updateEmployee( Employee employee) throws Throwable;
	
	
}
