package com.flexforce.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flexforce.dao.BasicCrud;
import com.flexforce.dto.CreateEmployeeDto;
import com.flexforce.entities.Employee;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;


@RestController
@RequestMapping("/employee")
@CrossOrigin
//@SecurityRequirement(name = "employee")
public class EmployeeController {

	@Autowired
	BasicCrud basicCrud ;
	
	@ApiResponse(description = "This api used for create new Employee")
	@PostMapping("/create")
	public String addNewEmployee(@RequestBody CreateEmployeeDto employee) {
	return basicCrud.addNewEmployee(employee);
	}
	
	@DeleteMapping("/delete/{empId}")
	public String deleteEmployee(@PathVariable  Long empId) throws Throwable{
	return basicCrud.deleteEmployee(empId);
	}
	
	@GetMapping("/{empId}")
	public Employee getEmployeeById(@PathVariable Long empId) throws Throwable {
		return basicCrud.getEmployeeById(empId);
	}
	
	@GetMapping("/name/{name}")
	public List<Employee> getEmployeeByName(@PathVariable String name) throws Throwable {
		return basicCrud.getEmployeeByName(name);
	}
	
	@PutMapping("/update")
	public String updateEmployee(@RequestBody Employee employee) throws Throwable {
		return basicCrud.updateEmployee(employee);
	}
	
}
