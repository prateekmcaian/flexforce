package com.flexforce.dto;

import javax.persistence.Column;

import lombok.Data;

@Data
public class CreateEmployeeDto {

	private String firstName;

	private String lastName;

	private Integer age;
	
	private Double salary;

}
